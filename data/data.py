from selenium.webdriver.common.by import By


class MainPageUrl:
    PAGE_URL = "https://runsmartproject.com/calculator/"


class MainPageLocators:
    # locators for values
    TIME_HOUR = (By.XPATH, "//input[@id='hr']")
    TIME_MIN = (By.XPATH, "//input[@id='min']")
    TIME_SEC = (By.XPATH, "//input[@id='sec']")

    # locator for button
    RESET_VALUES = (By.XPATH, "//a[@id='btnDelTime']")
    SUBMIT = (By.ID, "btnCalculate")

    # race data
    HOURS_FACT = "0"
    MIN_FACT = "60"
    SEC_FACT = "1"
    CURRENT_PACE = (7 * 60) + 55  # hours * 60 + sec

    # open training fields
    TRAINING = (By.ID, "ui-id-2")
    EASY_PACE = (By.XPATH, "//div[1]/div/div[3]/div[2]/table/tbody/tr[1]/td[4]")

    LINK_TO_INFO_PAGE = (By.CSS_SELECTOR, ".btn-about-vdot")


class InfoPageLocators:
    ABOUT_MODAL_WINDOW = (By.CSS_SELECTOR, "#about-vdot-modal")
