FROM python:3.11.2-slim

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .
