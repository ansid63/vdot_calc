from pages.main_page import MainPage
from pages.instraction_page import InstructionWindow
from data.data import MainPageUrl


class TestLinkVdotCalculator:
    def test_link(self, browser):
        link = MainPageUrl.PAGE_URL
        profile_page = MainPage(browser, link)
        profile_page.open()

        profile_page.go_to_instruction()
        instruction_page = InstructionWindow(browser, browser.current_url)
        instruction_page.check_about_modal_window()
