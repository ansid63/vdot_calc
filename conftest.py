import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options


@pytest.fixture(scope="function")
def browser():
    # for local selenoid launch with VNC
    # browser = webdriver.Remote(command_executor="http://localhost:4444/wd/hub",
    #                            desired_capabilities={"browserName": "firefox",
    #                                                  "browserVersion": "87.0",
    #                                                  "enableVNC": True,
    #                                                  "enableVideo": False})

    # for remote Firefox in CI GitLab
    # browser = webdriver.Remote(command_executor="http://selenoid__firefox:4444/wd/hub",
    #                            desired_capabilities=DesiredCapabilities.FIREFOX)

    # for remote in Moon
    chrome_options = ChromeOptions()
    desired_capabilities = {"screenResolution": "1280x1024",
                            "env": ["LANG=ru_RU.UTF-8", "LANGUAGE=ru:en", "LC_ALL=ru_RU.UTF-8", "TZ=Europe/Moscow"]}
    chrome_options.set_capability("browserName", "chrome")
    chrome_options.set_capability("moon:options", desired_capabilities)
    browser = webdriver.Remote(command_executor="http://192.168.39.207:4444/wd/hub",
                               options=chrome_options)

    # for local with driver
    # browser = webdriver.Chrome()

    # for remote Chrome in CI GitLab
    # chrome_options = Options()
    # chrome_options.add_argument("--no-sandbox")
    # browser = webdriver.Remote(command_executor="http://selenoid__chrome:4444",
    #                            options=chrome_options,
    #                            desired_capabilities=DesiredCapabilities.CHROME)
    browser.maximize_window()
    yield browser
    browser.quit()
