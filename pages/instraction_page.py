from pages.base_page import BasePage
from data.data import InfoPageLocators


class InstructionWindow(BasePage):
    def check_about_modal_window(self):
        modal_window = self.browser.find_element(*InfoPageLocators.ABOUT_MODAL_WINDOW)
        assert modal_window.is_enabled(), "Модальное окно описания не открылось"
