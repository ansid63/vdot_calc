from pages.base_page import BasePage
from data.data import MainPageLocators


class MainPage(BasePage):
    def go_to_instruction(self):
        info_page = self.browser.find_element(*MainPageLocators.LINK_TO_INFO_PAGE)
        info_page.click()
