**Project for testing Selenoid/Moon launching.**

1. docker-compoze.yml launch Seleniod Chrome with VNC locally.
   
    Use http://localhost:7070 in your browser if you want to look at browser actions.
2. .gitlab-ci.yml for remote launching tests with Firefox and Chrome at GitLab, choose one what needed.
3. Choose driver type in conftest.py: options Selenoid local, Selenoid Remote, Moon
4. For change basic Moon setting use 
```
helm upgrade --install -f custom-values.yaml -n moon moon aerokube/moon2 
```